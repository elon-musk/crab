package com.baomidou.crab.sys.mapper;

import com.baomidou.crab.sys.entity.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统参数表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2018-10-20
 */
public interface ParamMapper extends BaseMapper<Param> {

}
